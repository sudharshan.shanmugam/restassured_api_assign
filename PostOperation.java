package com.Api;

import org.testng.annotations.Test;

public class PostOperation {
	
	  @Test
	  public void postOperationTest() {
		  JSONObject request=new JSONObject();
		  request.put("name", "kiran");
		  request.put("job", "trainer");
		  
		  System.out.println(request);
		  
		  baseURI="https://reqres.in/api";
		  given().body(request.toJSONString()).when().post("/users").then().statusCode(201);
	  }
	}

}
