package com.Api;

public class TestingSingleUser {
	
	
	 @Test
	  public void testStatusCode() {
		  baseURI="https://reqres.in";
			given().get("/api/users/2").then().statusCode(200);
	  }
	  @Test
	  public void testparticularValue() {
		baseURI="https://reqres.in";
		given().get("/api/users/2").then().body("data.last_name", equalTo("Weaver"));
	  }
	  @Test
	  public void printAllValues() {
		baseURI="https://reqres.in";
		given().get("/api/users/2").then().log().all();
	  }
	  

}
