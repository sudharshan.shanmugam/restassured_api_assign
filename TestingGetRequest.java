package com.Api;

public class TestingGetRequest {
	
	 @Test
	  public void firstGetMethod() {
		  
		  Response response=RestAssured.get("https://reqres.in/api/users?page=2");
		  int statusCode=response.getStatusCode();
		  System.out.println(statusCode);
		  System.out.println(response.getBody().asString());
	  }
	  
	  @Test
	  public void assertGetMethod() {
		  
		  Response response=RestAssured.get("https://reqres.in/api/users?page=2");
		  int statusCode=response.getStatusCode();
		  Assert.assertEquals(200, statusCode);
	  }
	

}
